package see.exception;

import java.io.IOException;

public class SeeException extends RuntimeException {

    public SeeException(String message, IOException cause) {
        super(message, cause);
    }

    public SeeException(Throwable... throwableList) {
        super(merge(throwableList));
    }

    private static Throwable merge(Throwable... throwableList) {
        if (throwableList.length == 0) {
            return new Throwable();
        }
        Throwable previousThrowable = new Throwable(throwableList[0]);
        for (int i = 1; i < throwableList.length; i++) {
            previousThrowable = new Throwable(previousThrowable);
        }
        return previousThrowable;
    }
}
