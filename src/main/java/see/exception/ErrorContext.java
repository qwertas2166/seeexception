package see.exception;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

public class ErrorContext {
    private final List<Throwable> throwableList = new LinkedList<>();

    public void add(Throwable e) {
        this.throwableList.add(e);
    }

    public <Wrapper extends SeeException> Wrapper get(Class<Wrapper> wrapperClass) {
        try {
            return wrapperClass.getConstructor(Throwable.class).newInstance(throwableList.toArray(Throwable[]::new));
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isEmpty() {
        return throwableList.isEmpty();
    }
}
