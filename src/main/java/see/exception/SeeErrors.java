package see.exception;

import see.functional.CheckedAction;

public class SeeErrors {
    private static final ThreadLocal<ErrorContext> THREAD_EXCEPTION_ACCUMULATOR = ThreadLocal.withInitial(ErrorContext::new);

    public static Accumulator accumulator() {
        return new Accumulator();
    }

    public static class Accumulator {
        public Accumulator check(CheckedAction supplier) {
            ErrorContext errorContext = THREAD_EXCEPTION_ACCUMULATOR.get();
            try {
                supplier.act();
            } catch (Exception e) {
                errorContext.add(e);
            }
            return new Accumulator();
        }

        public <Wrapper extends SeeException> void throwErrorIfExist(Class<Wrapper> wrapperClass)  {
            ErrorContext errorContext = THREAD_EXCEPTION_ACCUMULATOR.get();
            if (!errorContext.isEmpty()) {
                throw errorContext.get(wrapperClass);
            }
        }
    }

}
